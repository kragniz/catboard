#!/usr/bin/env python3

import asyncio
import datetime
import hashlib
import json
import logging
import os
import tempfile
import pickle

from aiohttp import web

logging.basicConfig(level=logging.DEBUG)


class Thread:
    def __init__(self):
        self.posts = []
        self.post_number = 1


class Post:
    def __init__(self, message, image, image_hash, post_number, date,
                 name='Anonymous',):
        self.message = message
        self.image = image
        self.image_hash = image_hash
        self.post_number = post_number
        self.date = date
        self.name = name

    def get_dict(self):
        return {
            'message': self.message,
            'image': self.image,
            'date': self.date.isoformat().split('.')[0],
            'post': str(self.post_number),
            'name': self.name,
        }


# TODO: use an actual db instead of this crap
if os.path.isfile('posts.ktty'):
    with open('posts.ktty', 'rb') as f:
        thread = pickle.load(f)
else:
    thread = Thread()


async def get_handler(request):
    name = request.match_info.get('file', None)
    if name is None:
        name = 'index.html'
    if name in {'index.html', 'mustache.js.min'}:
        with open(name) as f:
            body = f.read()
        return web.Response(body=body.encode('utf-8'),
                            content_type='text/html')
    else:
        return web.Response(status=404)


def save_file(file, ext):
    fd, file_name = tempfile.mkstemp()
    temp_file = os.fdopen(fd, 'wb')

    sha256 = hashlib.sha256()

    while True:
        data = file.read(65536)
        if not data:
            break
        sha256.update(data)

        temp_file.write(data)

    file.close()
    temp_file.close()

    hash = sha256.hexdigest()[:16]

    new_name = os.path.join('i', hash + ext)
    os.rename(file_name, new_name)
    return new_name, hash


async def post_handler(request):
    app = request.app
    post = await request.post()
    message = post['message']
    image = post['image']

    _, ext = os.path.splitext(str(image.filename))
    print(_, ext)
    image_name, hash = save_file(image.file, ext)

    for post in thread.posts:
        if post.image_hash == hash:
            msg = 'Image already exists in this thread'
            return web.Response(status=409,
                                text=json.dumps({'message': msg}))

    new_post = Post(message, image_name, hash, thread.post_number,
                    datetime.datetime.utcnow(),
                    'Anonymous')
    thread.post_number += 1

    thread.posts.append(new_post)

    with open('posts.ktty', 'wb') as f:
        pickle.dump(thread, f)

    for ws in app["sockets"]:
        ws.send_str(json.dumps(new_post.get_dict()))

    return web.Response(status=200,
            text=json.dumps({'post_number': new_post.post_number}))


async def wshandler(request):
    app = request.app
    ws = web.WebSocketResponse()
    await ws.prepare(request)
    app['sockets'].append(ws)

    for post in thread.posts:
        ws.send_str(json.dumps(post.get_dict()))

    async for msg in ws:
        if msg.tp == aiohttp.MsgType.text:
            print('text')
            if msg.data == 'all':
                print('all')
                for post in thread.posts:
                    ws.send_str(json.dumps(post.get_dict()))
        if msg.tp == web.MsgType.close:
            break

    app['sockets'].remove(ws)
    return ws


async def init(loop):
    app = web.Application(loop=loop)
    app.router.add_route('GET', '/ws', wshandler)
    app.router.add_route('GET', '/{file}', get_handler)
    app.router.add_static('/i', 'i')
    app.router.add_route('GET', '/', get_handler)
    app.router.add_route('POST', '/post', post_handler)
    app['sockets'] = []
    app['post_number'] = 1

    srv = await loop.create_server(app.make_handler(), '0.0.0.0', 8080)
    return srv


loop = asyncio.get_event_loop()
loop.run_until_complete(init(loop))
loop.run_forever()
